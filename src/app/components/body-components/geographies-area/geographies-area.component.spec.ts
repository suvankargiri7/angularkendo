import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeographiesAreaComponent } from './geographies-area.component';

describe('GeographiesAreaComponent', () => {
  let component: GeographiesAreaComponent;
  let fixture: ComponentFixture<GeographiesAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeographiesAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeographiesAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
