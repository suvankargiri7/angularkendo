import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeographiesCategoryComponent } from './geographies-category.component';

describe('GeographiesCategoryComponent', () => {
  let component: GeographiesCategoryComponent;
  let fixture: ComponentFixture<GeographiesCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeographiesCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeographiesCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
