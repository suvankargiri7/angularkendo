import { Component, OnInit } from '@angular/core';
import { SelectableSettings } from '@progress/kendo-angular-grid';
import { products } from './mocks/products';

@Component({
  selector: 'app-geographies-category',
  templateUrl: './geographies-category.component.html',
  styleUrls: ['./geographies-category.component.scss']
})
export class GeographiesCategoryComponent implements OnInit {

  public gridData: any[] = products;
  public actionItems: Array<Object> = [
    {
      actionId: "1",
      actionName: "Delete",
    }
  ];
  public actionItemPlaceholder: Object = { actionName: 'Bulk Action', actionId: null };
  public checkboxOnly = false;
  public mode = "multiple";
  public selectableSettings: SelectableSettings;
  public hideAddGeographyCategoryForm = true;
  constructor() {
    this.setSelectableSettings();
  }

  ngOnInit() {
  }

  public setSelectableSettings(): void {
    this.selectableSettings = {
      checkboxOnly: this.checkboxOnly,
      mode: 'multiple'
    };
  }

  public showAddGeographyCategoryForm(): void {
    this.hideAddGeographyCategoryForm = !this.hideAddGeographyCategoryForm;
  }

}
