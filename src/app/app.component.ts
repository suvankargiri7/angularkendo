import { Component, ViewEncapsulation  } from '@angular/core';
import { DrawerItem, DrawerSelectEvent } from '@progress/kendo-angular-layout';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
  title = 'angularKendo';
  public expanded = true;
  public items: Array<any> = [];
  public selected = 0;
  public showAllSubmenuBoolean = false;
    constructor(private router: Router) {
        const routes: any[] = router.config;

        routes.forEach((route) => {
          if(route.hasOwnProperty('redirectTo')==false){
            this.items.push({
              text: route.text ? route.text: '',
              path: route.path ? route.path : '',
              icon: route.icon ? route.icon : '',
              children: route.children ? route.children : [],
              redirectTo: route.redirectTo ? route.redirectTo: ''
            });
          } 
        });
        console.log(this.items);
        this.items[this.selected].selected = true;
    }

    public onSelect(ev: DrawerSelectEvent): void {
      console.log(ev);
      this.selected = ev.index;
      if(ev.item.children.length > 0) {
        if(!ev.item.hasOwnProperty('showSubmenu')) {
          ev.item.showSubmenu = true;
        }
        else{
          delete ev.item.showSubmenu;
        }
      }
      console.log(ev);
    }

    public gotoUrl(parentPath: string, childPath: string): void {
      this.router.navigateByUrl(`/${parentPath}/${childPath}`);
    }

    public showAllSubmenu(): void {
      this.showAllSubmenuBoolean = true;
    }

    public hideCurrentOpenSubmenu(): void {
      this.showAllSubmenuBoolean = false;
      const cols = document.querySelectorAll(".submenu");
      console.log(cols);
    }
}
