import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  DashboardComponent, 
  GeographiesComponent, 
  GeographiesCategoryComponent,
  GeographiesAreaComponent
 } from './components/body-components';

const routes = [
  {
    path: '',
    redirectTo:'users',
    pathMatch: 'full'
  },
  {
    path: 'geographies',
    text: 'Manage Geographies',
    icon: 'k-i-globe-outline',
    children: [
      { path: '', redirectTo:'categories',pathMatch: 'full'},
      { path: 'categories', text: 'Geography Catrgoies', component: GeographiesCategoryComponent,pathMatch: 'full' },
      { path: 'area', text: 'Geography Area', component: GeographiesAreaComponent ,pathMatch: 'full' },
    ]
  },
  {
    path: 'users',
    component: DashboardComponent,
    text: 'Manage Users',
    icon: 'k-i-user',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
